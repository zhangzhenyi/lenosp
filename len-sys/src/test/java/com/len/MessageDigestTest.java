package com.len;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.util.ByteSource;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author: 张天敏
 * @date: 2019/11/12 15:58
 * @description: MessageDigest加密测试
 **/
@Slf4j
public class MessageDigestTest {

    /**
     * 盐值计算
     * @param username 用户名
     * @return byte[] 盐值byte数组
     */
    public byte[] getSalt(String username){
        // 计算盐值
        ByteSource byteSource = ByteSource.Util.bytes(username);
        return byteSource.getBytes();
    }

    /**
     * 将字符串转换成byte数组
     * @param str 输入字符串
     * @return byte[]
     */
    private byte[] getByte(String str) {
        return str.getBytes();
    }

    /**
     * 测试md5盐值加密算法
     * 用户admin，密码123456
     * 加密后的结果：e0b141de1c8091be350d3fc80de66528
     */
    @Test
    public void testMD5(){
        // 计算次数
        Integer hashIterations = 4;
        try {
            // 装载md5加密方法
            MessageDigest digest = MessageDigest.getInstance("md5");
            // 计算用户盐值
            byte[] salt = getSalt("22222");
            // 将密码转换成byte数组
            byte[] bytes = getByte("123456");
            // 如果盐值不为空，则MessageDigest加载盐值
            if (salt != null) {
                digest.reset();
                digest.update(salt);
            }
            // 执行md5运算
            byte[] hashed = digest.digest(bytes);
            int iterations = hashIterations - 1; //already hashed once above
            //iterate remaining number:
            for (int i = 0; i < iterations; i++) {
                digest.reset();
                hashed = digest.digest(hashed);
            }
            // 将byte数组转换成字符串
            String result = byteToString(hashed);
            // 在控制台中打印加密后的md5字符串
            log.info("md5加密结果：{}",result);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * 将byte数组转换成字符串
     * @param hashed 加密字符串
     * @return String
     */
    private String byteToString(byte[] hashed) {
        // 创建字符串StringBuffer
        StringBuffer sb = new StringBuffer();
        // 创建缓存
        String temp = null;
        // 循环遍历拼接字符串
        for(int i = 0,hashedLenth = hashed.length; i < hashedLenth; i ++){
            temp = Integer.toHexString(0xFF & hashed[i]);
            sb.append(temp);
        }
        return sb.toString();
    }

}
