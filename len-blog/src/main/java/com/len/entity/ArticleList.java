package com.len.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zhuxiaomeng
 * @date 2018/10/1.
 * @email 154040976@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleList {

    private String id;

    private String code;

    private String title;

    private Integer topNum;

    private String createBy;

    private String createDate;

    private String content;

}
